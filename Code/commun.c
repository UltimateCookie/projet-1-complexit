#include "commun.h"

/*
 * name: ajout_elt_clique
 * Fonction qui permet d'ajouter un sommet dans une clique.
 * @param Une clique et un entier.
 * @return 1 Si l'ajout à fonctionné, 0 sinon.
 */
int ajout_elt_clique(clique* c, int x) {
	//parourir la clique et vérifier si x n'yest pas déja
		
/*	if(c.nb_elt == n_max-1) // verifie si on ne déborde pas de la clique
		return 0;
	c.elt[c.nb_elt]=x;
	c.nb_elt++;*/
	/*while(x<=n_max)
	{	
    	if(c.elt[x]==1)
    	  return 0;
    	else
    	  c.elt[c.nb_elt]=x;
    	  c.nb_elt++;
    	  x=x+1;
	}*/
	
	if(c->elt[x]==1 || x<0)
		return 0;
		
	
	c->elt[x]=1;
	c->nb_elt++;		
	return 1;
}


int enlever_elt_clique(clique* c, int x) {
		
/*	c.elt[c.nb_elt]=x;
	c.nb_elt++;
	while(x<=n_max)
	{	
    	if(! c.elt[x])
    	  return 0;
    	else
    	  c.elt[c.nb_elt]=0;
    	  c.nb_elt--;
    	  x=x+1;
	}*/
	
	
	if(c->elt[x]==0)
	return 0;
	
	c->elt[x]=0;
	c->nb_elt--;		
	return 1;
	
	
}

int arc2 (liste l, sommet x) {
	//a coder
}



int arc (graphe_l g, sommet x, sommet y)
{
    
    liste l;
    l= g.a[x];
    while ((l!= NULL)&&(l->st != y))
        l=l->suivant;
    return (l!= NULL);
}


/*
 * 
 * name: copie
 * @param une clique
 * @return la copie de cette clique
 * 
 */
clique copie_clique (clique* c2) {
	int i = 0;
	clique c;
	
	for ( i=0; i< n_max; i++) {
		
		c.elt[i] = c2->elt[i];
			
	}
	
	c.nb_elt = c2->nb_elt;
	return c;
}


/*
 * 
 * name: vider_clique
 * @param une clique
 * @return
 * 
 */
void vider_clique (clique* c) {
	
	int i;
		
	for ( i=0; i< n_max; i++) {
		c->elt[i]=0;
	}
	
	c->nb_elt = 0;
}

void clique_afficher(clique* c) {
	if(c->nb_elt ==0) {
		printf("Clique vide\n");
		return;
	}
	printf("Affichage de clique de %i elements\n", c->nb_elt);
	int i;
	for ( i=0; i< n_max; i++) {
		if(c->elt[i])
			printf(" %i ", i);
	}
	printf("\n Fin affichage \n");
	
}


void afficher_graphe(graphe_l g) {
	int i;
    liste p;
    for(i = 0; i<g.n; i++){
        p = g.a[i];
        printf("%d : ",i);
        while(p!=NULL){
            printf("%d, ",p->st);
            p = p->suivant;
        }
        printf("\n");
    }    
}	

void afficher_coloration(coloration *c) {
	int i;
	for(int i=0; i<c->nb_elt; i++) {
		printf("sommet %d: Couleur %d \n", i, c->elt[i]);
	}
}
	
