#ifndef LECTURE
#define LECTURE

#include "commun.h"


#include <stdlib.h>
#include <stdio.h>

graphe_l init_graphe(graphe_l G);
graphe_l ajouter_sommet(graphe_l G);
int ajout_arete_possible(graphe_l G, sommet s1, sommet s2);
graphe_l ajouter_arete(graphe_l G, sommet s1, sommet s2);
graphe_l lire_graphe( FILE *f, graphe_l g);

#endif
