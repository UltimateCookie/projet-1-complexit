#include "calc_clique_maximale.h"


void calcul_clique_maximale( graphe_l g, clique* c) { //clique c doit etre vide
	
	if(c->nb_elt !=0) {
		printf("Erreur: la clique doit etre vide.");
		return;
	}

	clock_t time = clock();
	
	ajout_elt_clique(c,0); // un seul sommet dans la clique ajouté abritairement: le sommet 0
	calcul_clique_maximale_rec(g,c);

	time = clock()-time;
	printf("[calc_clique_maximale] Temps d'execution : %fs\n",(double)time/CLOCKS_PER_SEC);
	
}


int calcul_clique_maximale_rec( graphe_l g, clique* c) { //fonction récurisive
	int x=0;
	int ok=0;
	
	while ( x < g.n && !ok) { //parcours des sommets du graphes
		
		if( ajout_elt_clique (c, x) ) { //on ajoute un sommet
			
			if( maximalite(g, *c) ) {//si on obtient une maximalité, c'est bon.
				ok=1;
			}
		
		else {
			
			if( verification(g, *c) && calcul_clique_maximale_rec(g,c)) { //sinon on vérifie que c toujours une 
			//clique de g, et, comme x n'était pas suffisant pour faire une clique maximale,
			//on cherche à ajouter encore un sommet dans la clique afin qu'elle devienne maximale.
				ok=1;
			}
			
			if(!ok) {//sinon on peut enlever ce sommet x et essayer le suivant.
				enlever_elt_clique(c, x);
			}
		}
	}
		
	x = x+1;
	}
	return ok;
}
