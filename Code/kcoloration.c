#include "kcoloration.h"


int kVerification(graphe_l g, int x, int k, coloration* c) {
	
	int ok;
	liste p;
	ok=1;
	p = g.a[x];
	while( p!= NULL && ok) {
		
		
	printf( " couleur sommet %d = %d  ==  couleur voisin %d = %d \n", x,c->elt[x], p->st, c->elt[p->st]);
		
		if(c->elt[x] == c->elt[p->st] ) {
			ok=0; 
		}
	
		p = p ->suivant;
	}
	
	printf ( "   -> %d \n", ok);
	
	return ok;
}


int kColoration( graphe_l g, int k, coloration *c) {
	
	int ok;
	
	clock_t time = clock();
	
	ok= kColorationRec(g,0,k,c);
	
	time = clock()-time;
	printf("[Kcoloration] Temps d'execution : %fs\n",(double)time/CLOCKS_PER_SEC);
	
	return ok;
}


int kColorationRec(graphe_l g, sommet x, int k, coloration *c) {
	int i, ok, dejaColore;
	liste p;
	p=g.a[x];
	
	//Cond arret: si tous les sommets sont colorés.
	ok=1;
	i=0;
	while(i<g.n && ok) {
		if(c->elt[i]==0)
			ok=0;
		i=i+1;
	}
	i=1;
	
	dejaColore=(!c->elt[x]==0);
	while(i<=k && !ok) { 	//on parcours la liste des couleurs et on essaie de colorer x
		
		if(!dejaColore) { //si il n'est pas déja coloré
			c->elt[x]=i;
			printf("test sommet %d couleur %d \n", x, i);
		
			
			if(kVerification(g,x,k,c)) {//on verifie si la nouvelle couleur associée a x est bonne
				
				while(p != NULL && !ok ) { //on parcours ses voisins
					
					ok = kColorationRec(g, p->st, k, c); //appel récursif sur ses voisins
					
					p = p -> suivant;
				}
			}
			
		}
		i = i + 1;	
	}
	
	
	
	return ok;
}
		
	








