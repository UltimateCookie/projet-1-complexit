#ifndef KCOLORATION
#define KCOLORATION
#include "commun.h"

int kVerification(graphe_l g, int x, int k, coloration* c);
int kColoration( graphe_l g, int k, coloration *c);
int kColorationRec(graphe_l g, sommet x, int k, coloration *c);

#endif
