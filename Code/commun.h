#ifndef COMMUN
#define COMMUN


#include "time.h"
#include <stdlib.h>
#include <stdio.h>
#define n_max 6000

typedef int sommet;

typedef struct
	{
		int n;		        // Nombre réel de sommets, numérotés de 0 à n-1
		int a[n_max][n_max];	// Représente les arrêtes dans le graphe
	} graphe_n;



typedef struct chainon			// Liste de sommets
	{
		sommet st;
		struct chainon *suivant;
	}couple;


typedef couple *liste;


typedef struct
	{
		int n;			         // idem graphe-n
		liste a[n_max];			// liste de listes chainées
	} graphe_l;


typedef struct { //représente une clique
	int nb_elt;
	int elt[n_max];
} clique;

typedef clique coloration;


int ajout_elt_clique(clique* c, int x);
int enlever_elt_clique(clique* c, int x);
int arc2 (liste l, sommet x);
int arc (graphe_l g, sommet x, sommet y);
clique copie_clique (clique* c2);
void vider_clique (clique* c);
void clique_afficher(clique* c);
void afficher_graphe(graphe_l g);
void afficher_coloration(coloration *c);

#endif
