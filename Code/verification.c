#include "verification.h"

/*
 * name: verification
 * Fonction qui prend en entrées un graphe non orienté G et un ensemble de sommet X
 * et qui vérifie sur X est une clique de G.
 * @param Un graphe sous forme de liste, une clique.
 * @return 1 si la clique est une clique du graphe.
 */

int verification(graphe_l g, clique c) {
	
	if(c.nb_elt==0)
		return 0;
	
	int ok = 1;
	int i = 0, x = 0, y = 0, j = 0;
	liste p = NULL;
	
	clock_t time = clock();
	
	while ( i < n_max && ok ) { //on parcours la clique.
		if(c.elt[i]) { //pour trouver les elt à 1 dans la clique
			x = i;
			p = g.a[x]; //on récupère les voisins du sommet x extrait dans la clique.
			j = i + 1; //on regarde si les sommets suivants de la clique sont bien dans les voisins de x.
			while ( j < n_max && ok ) { //parcours 2 de la clique
				if(c.elt[j]) {
					y = j;
					if ( !arc ( g, x, y ) ) {
						ok=0; //si ce n'est pas le cas, on sort.
					}
				}
				j=j+1;	
			}
		}
		i=i+1;
	}
	
	time = clock()-time;
	printf("[verification] Temps d'execution : %fs\n",(double)time/CLOCKS_PER_SEC);

	return ok;
}









