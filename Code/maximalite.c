
#include "maximalite.h"

/*
 * name: maximalite
 * Fonction qui prend en entrées un graphe non orienté G et un ensemble de sommet X
 * et qui vérifie sur X est une clique maximale de G.
 * @param Un graphe sous forme de liste, une clique.
 * @return 1 si la clique est une clique maximale du graphe.
 */
int maximalite(graphe_l g, clique c) {
	
	sommet x;
	int ok;
	
	clock_t time = clock();
		
	if( verification(g,c) ) { //si la clique c est bien une clique de g
		x=0;
		ok=1;
		do { //on essaie de rajouter un sommet à cette clique
			if ( ajout_elt_clique (&c, x) ) {
				if ( verification (g, c) ) { //et on verifie si cette nouvelle clique est une clique.
					ok = 0; //si c'est le cas, il faut arreter le programme car on avait pas une clique maximale.
				}
				enlever_elt_clique (&c,x);//après avoir testé un sommet, il faut l'enlever pour pouvoir en tester un autre
			}
			x=x+1;
			
		} while (x < g.n && ok );
		
		
		time = clock()-time;
		printf("[maximalité] Temps d'execution : %fs\n",(double)time/CLOCKS_PER_SEC);
		
		return ok;
	}
	
	
	printf("Maximalité impossible car la clique donnée n'est pas une clique du graphe g \n");
	return 0;
}
