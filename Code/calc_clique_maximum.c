#include "calc_clique_maximum.h"



void calcul_clique_maximum( graphe_l g, clique* c2) { //clique c doit etre vide
	
	clique c_max;
	c_max = copie_clique(c2);
	int i;

	clock_t time = clock();
	
	for(i=0; i<g.n; i++) { //pour chaque sommet
		ajout_elt_clique(c2,i);
		calcul_clique_maximale_rec(g,c2); //on cherche la clique maximale
		printf ("c2: %i, c_max: %i\n", c2->nb_elt, c_max.nb_elt);
		if(c2->nb_elt > c_max.nb_elt ) { //et on conserve la plus grande clique qu'on ait trouvé
			c_max = copie_clique(c2);
		}
		
		vider_clique(c2);
		
	}
	
	*c2=c_max; //on retourne dans c2 la plus grande clique que l'on ait trouvé.

	time = clock()-time;
	printf("[calc_clique_maximum] Temps d'execution : %fs\n",(double)time/CLOCKS_PER_SEC);
	
	
}
