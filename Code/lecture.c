
 /* Initisalisation de struture graphe_l (listes d'adjacences) 
 * @param G graphe non-orienté 
 * @complexite O(1)
 */
#define VRAI 1
#define FAUX 0

#include "lecture.h"

graphe_l init_graphe(graphe_l G){	
	G.n = 0;
	G.a[G.n] = NULL;
	return G;	
}

/** 
 * Ajout de sommet dans le graphe 
 * @param G graphe non-orienté 
 * @complexite O(1)
 */
graphe_l ajouter_sommet(graphe_l G){
	G.a[G.n] = NULL;
	G.n ++;
	return G;
}

/** 
 * Fonction qui vérifie si l'on peut ajouter une arete entre deux sommets
 * @param G graphe non-orienté 
 * @param s1 sommet
 * @param s2 sommet
 * @complexite O(n)
 */
int ajout_arete_possible(graphe_l G, sommet s1, sommet s2){
	
	if( (s1 < G.n) && ( s1 > -1 ) ){
		if(( s2 < G.n) && (s2 > -1)){
			liste l;
			l = G.a[s1]; 
			while (l != NULL){
				if(l->st == s2){
					printf("Attention : %d -> %d exite déja!\n", s1,s2);
					return FAUX;
				}else{
					l = l->suivant;
				}
			}
			return VRAI; 
		}
		else{
			//printf("\n ereeurr %d \n",s2);
			return FAUX;
		}
	}
	else{
		//printf("\nsommet inexistant %d \n",s1);
		return FAUX;
	}
}

/**
 * Fonction qui ajoute une arete entre deux sommets
 * @param G graphe non-orienté
 * @param s1 sommet
 * @param s2 sommet
 * @complexite en O(n)
 */
graphe_l ajouter_arete(graphe_l G, sommet s1, sommet s2){ 
	
	if( ajout_arete_possible(G, s1, s2) == VRAI ){
		liste l,l2;
		l=malloc(sizeof(liste));
		l->st=s2;
		l->suivant=G.a[s1];
		G.a[s1] = l;
		
		l2=malloc(sizeof(liste));
		l2->st=s1;
		l2->suivant=G.a[s2];
		G.a[s2] = l2;
	}
		
	return G;
}

/**
 * name :Lecture d'un graphe à partir d'un fichier 
 * @param f Fichier
 * @param g graphe non-orienté 
 * @complexite en O(n)
 */
graphe_l lire_graphe( FILE *f, graphe_l g)
{
	int x, y, i;
	g = init_graphe(g); 
	
	// première ligne : nombre_de_sommet nombre_d'aretes
	if( fscanf(f, "%d %d", &x, &y))	
		for(i =0; i< x; i++ ){
			g = ajouter_sommet(g);	
		}
		
		printf("\n Nombre_de_sommet %d, nombre_d'aretes %d ", g.n, y);
		while( fscanf(f,"%d %d",&x, &y) ==2 ){	
			//fscanf retourne le nombre de variable lues.
			g = ajouter_arete(g, x, y);	
		}
	return g;			
}
