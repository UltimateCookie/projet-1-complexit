#ifndef CALC_CLIQUE_MAXIMALE
#define CALC_CLIQUE_MAXIMALE
#include "commun.h"
#include "verification.h"
#include "maximalite.h"

void calcul_clique_maximale( graphe_l g, clique* c);
int calcul_clique_maximale_rec( graphe_l g, clique* c);

#endif

