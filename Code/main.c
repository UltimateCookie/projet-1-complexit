#include <stdio.h>
#include <stdlib.h>
#include "commun.h"
#include "calc_clique_maximum.h"
#include "kcoloration.h"
#include "calc_clique_maximum.h"
#include "calc_clique_maximale.h"
#include "verification.h"
#include "maximalite.h"
#include "lecture.h"

#define VRAI 1
/********************************* MAIN *******************************/
int main(int argc, char **argv)
{

	
	FILE *f = NULL; 
	int choix, k;
	graphe_l gl;
	clique c , c2;
	coloration co;
	
	//init clique
	int i;
	for ( i=0; i< n_max; i++) {
		c.elt[i]=0;
		c2.elt[i]=0;
		co.elt[i]=0; //coloration
	}
	c.nb_elt = 0;
	c2.nb_elt = 0;
	co.nb_elt = 0;


	//lire le fichier
	if( (f = fopen(argv[1], "r")) ){
		gl = lire_graphe(f,gl);
		fclose(f);	
	}

	else{
		fprintf(stderr, "impossible d'ouvrir le fichier %s\n", argv[1]);
		exit(1);
	}
	
	choix=0;
	
	//entrer une clique
	printf("\n \nVeuillez entrer une clique \n");
	while(choix != -1) {
		printf("entrer un sommet pour ajouter des elements, entrer -1 pour arreter \n");
		scanf("%d",&choix);
		if(choix!=-1) {
			if(ajout_elt_clique(&c,choix))
				printf("Ajout ok\n");
			else
				printf("Ajout impossible\n");
		}
		else
			printf("Ajout impossible\n");
	}
	
	
	choix=0;

	//prgm
	do{
		printf("\n ***********************  Choix ***************** \n");
		printf("\t 1. :\t AFFICHER LE GRAPHE \n");
		printf("\t 2. :\t AFFICHER LA CLIQUE \n"); 
		printf("\t 3. :\t VÉRIFICATION \n"); 
		printf("\t 4. :\t MAXIMALITÉ \n");  
		printf("\t 5. :\t MAXIMALE \n");  
		printf("\t 6. :\t MAXIMUM \n");   
		printf("\t 7. :\t K-COLORATION \n"); 
		printf("\t 0. :\t QUITTER \n"); 
		
		printf("\n veuillez choisir le numéro de la partie que vous voulez tester\n");
		scanf("%d",&choix);
		
		switch(choix){
			
			case 1:
			
				printf("Affichage du graphe \n");
				afficher_graphe(gl);
				break;
				
			case 2:
				clique_afficher(&c);
				break;
				
			case 3:
				if( verification(gl,c) )
					printf("La clique est une clique du graphe \n");
				else
					printf("La clique n'est pas une clique du graphe \n");
				break;
				
			case 4:
				if( maximalite(gl,c) )
					printf("La clique est une clique maximale du graphe \n");
				else
					printf("La clique n'est pas une clique maximale du graphe \n");
				break;
				
			case 5:
				vider_clique(&c2);
				calcul_clique_maximale(gl,&c2);
				printf("Voici une clique maximale de g à partir du sommet 0:\n");
				clique_afficher(&c2);
				break;
				
			case 6:
				vider_clique(&c2);
				calcul_clique_maximum(gl,&c2);
				printf("Voici la clique maximum de g:\n");
				clique_afficher(&c2);
				break;

			case 7:
				co.nb_elt= gl.n;
				printf("entrer le nombre k de la couleur \n");
				scanf("%d",&k);
				if( kColoration(gl,k,&co) )
					printf("Voici une coloration:\n");
				else
					printf("Aucune coloration possible\n");	
				afficher_coloration(&co);				
				break;
				
			default:
				choix=0;
				break;
				
			break;
			
		}
		printf("\n \n \n");
	}while(choix != 0);

return 1;
}
