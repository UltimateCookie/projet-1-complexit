Bonjour.

Notre programme se trouve dans le dossier "Code".

Il suffit d'utiliser le makefile:

make

Et de lancer le programme avec le graphe passé en argument:

./programme ../Benchs/1-FullIns_3

ou par exemple: 

./programme ../generateur/graph_200_2000_0

nmax (situé dans Code/commun.h) est défini à 6000.
Pour avoir plus de sommets ou d'arc, il faut augmenter cette valeur.


Dans le dossier Code, le graphe "graphe_test.txt" représente le graphe que nous avons expliqué en cours.
./programme graphe_test.txt

Lors de l'exécution, si vous ne souhaitez pas entrer de clique, il suffit de faire -1 directement.

Le code du projet se trouve aussi en ligne à l'adresse suivante:
https://bitbucket.org/UltimateCookie/projet-1-complexit

Un exemple d'execution:

loic@GE62-6QD:~/Desktop/projet-1-complexit/Code$ make
gcc -c main.c -o main.o
gcc -c verification.c -o verification.o
gcc -c maximalite.c -o maximalite.o
gcc -c calc_clique_maximale.c -o calc_clique_maximale.o
gcc -c commun.c -o commun.o
gcc -c lecture.c -o lecture.o
gcc -c calc_clique_maximum.c -o calc_clique_maximum.o
gcc main.o verification.o maximalite.o calc_clique_maximale.o commun.o lecture.o calc_clique_maximum.o -o programme
loic@GE62-6QD:~/Desktop/projet-1-complexit/Code$ ./programme graphe_test.txt 

 Nombre_de_sommet 5, nombre_d'aretes 6 
 
Veuillez entrer une clique 
entrer un sommet pour ajouter des elements, entrer -1 pour arreter 
2
Ajout ok
entrer un sommet pour ajouter des elements, entrer -1 pour arreter 
3
Ajout ok
entrer un sommet pour ajouter des elements, entrer -1 pour arreter 
-1
Ajout impossible

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
1
Affichage du graphe 
0 : 4, 3, 1, 
1 : 2, 0, 
2 : 3, 1, 
3 : 4, 2, 0, 
4 : 3, 0, 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
2
Affichage de clique de 2 elements
 2  3 
 Fin affichage 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
3
[verification] Temps d'execution : 0.000174s
La clique est une clique du graphe 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
4
[verification] Temps d'execution : 0.000198s
[verification] Temps d'execution : 0.000009s
[verification] Temps d'execution : 0.000016s
[verification] Temps d'execution : 0.000014s
[maximalité] Temps d'execution : 0.000349s
La clique est une clique maximale du graphe 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
5
[verification] Temps d'execution : 0.000215s
[verification] Temps d'execution : 0.000012s
[verification] Temps d'execution : 0.000071s
[verification] Temps d'execution : 0.000071s
[maximalité] Temps d'execution : 0.000536s
[calc_clique_maximale] Temps d'execution : 0.000563s
Voici une clique maximale de g à partir du sommet 0:
Affichage de clique de 2 elements
 0  1 
 Fin affichage 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
6
[verification] Temps d'execution : 0.000266s
[verification] Temps d'execution : 0.000014s
[verification] Temps d'execution : 0.000079s
[verification] Temps d'execution : 0.000078s
[maximalité] Temps d'execution : 0.000532s
c2: 2, c_max: 0
[verification] Temps d'execution : 0.000219s
[verification] Temps d'execution : 0.000012s
[verification] Temps d'execution : 0.000078s
[verification] Temps d'execution : 0.000079s
[maximalité] Temps d'execution : 0.000449s
c2: 2, c_max: 2
[verification] Temps d'execution : 0.000007s
Maximalité impossible car la clique donnée n'est pas une clique du graphe g 
[verification] Temps d'execution : 0.000007s
[verification] Temps d'execution : 0.000192s
[verification] Temps d'execution : 0.000013s
[verification] Temps d'execution : 0.000012s
[verification] Temps d'execution : 0.000014s
[maximalité] Temps d'execution : 0.000294s
c2: 2, c_max: 2
[verification] Temps d'execution : 0.000192s
[verification] Temps d'execution : 0.000079s
[verification] Temps d'execution : 0.000007s
[verification] Temps d'execution : 0.000261s
[maximalité] Temps d'execution : 0.000596s
[verification] Temps d'execution : 0.000190s
[verification] Temps d'execution : 0.000127s
Maximalité impossible car la clique donnée n'est pas une clique du graphe g 
[verification] Temps d'execution : 0.000078s
[verification] Temps d'execution : 0.000007s
Maximalité impossible car la clique donnée n'est pas une clique du graphe g 
[verification] Temps d'execution : 0.000006s
[verification] Temps d'execution : 0.000269s
[verification] Temps d'execution : 0.000089s
[verification] Temps d'execution : 0.000009s
[maximalité] Temps d'execution : 0.000427s
c2: 3, c_max: 2
[verification] Temps d'execution : 0.000191s
[verification] Temps d'execution : 0.000079s
[verification] Temps d'execution : 0.000006s
[verification] Temps d'execution : 0.000260s
[maximalité] Temps d'execution : 0.000596s
[verification] Temps d'execution : 0.000190s
[verification] Temps d'execution : 0.000081s
Maximalité impossible car la clique donnée n'est pas une clique du graphe g 
[verification] Temps d'execution : 0.000078s
[verification] Temps d'execution : 0.000007s
Maximalité impossible car la clique donnée n'est pas une clique du graphe g 
[verification] Temps d'execution : 0.000006s
[verification] Temps d'execution : 0.000264s
[verification] Temps d'execution : 0.000086s
[verification] Temps d'execution : 0.000007s
[maximalité] Temps d'execution : 0.000401s
c2: 3, c_max: 3
[calc_clique_maximum] Temps d'execution : 0.004841s
Voici la clique maximum de g:
Affichage de clique de 3 elements
 0  3  4 
 Fin affichage 

 
 

 ***********************  Choix ***************** 
	 1. :	 AFFICHER LE GRAPHE 
	 2. :	 AFFICHER LA CLIQUE 
	 3. :	 VÉRIFICATION 
	 4. :	 MAXIMALITÉ 
	 5. :	 MAXIMALE 
	 6. :	 MAXIMUM 
	 0. :	 QUITTER 

 veuillez choisir le numéro de la partie que vous voulez tester
0

loic@GE62-6QD:~/Desktop/projet-1-complexit/Code$ make clean
rm -rf *.o
rm -rf programme


